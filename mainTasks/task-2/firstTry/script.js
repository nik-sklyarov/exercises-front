let cells = document.querySelectorAll(".cell");
let menu = document.querySelector(".menu");
let alert = document.createElement('h3');
let aiGame = false;
let aiEasy = true;
let firstPlayerTurn = true;
let winner = null;
const winCombos = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [6,4,2]
]

function startGame () {
    menu.style.display = 'none';
    alert.innerText = '';
    firstPlayerTurn = true;
    winner = null;
    for ( cell of cells ) {
        cell.empty = true;
        cell.owner = null;
        cell.firstElementChild.innerText = "";
        cell.addEventListener('click', click);
    }
}

function click (event) {
    if ( aiGame === true ) {
        if ( this.empty === true && firstPlayerTurn === true ) {
            this.empty = !this.empty;
            this.owner = 'player1';
            this.firstElementChild.innerText = 'X';
            checkWin(cells);
            checkTie(cells);
            aiTurnEasy();
            checkWin(cells);
            checkTie(cells);
        }
    } else {
        if (this.empty === true && firstPlayerTurn === true) { 
            this.empty = !this.empty;
            this.owner = 'player1';
            this.firstElementChild.innerText = 'X';
            firstPlayerTurn = !firstPlayerTurn;
        } else if ( this.empty === true && firstPlayerTurn === false) {
            this.empty = !this.empty; 
            this.owner = 'player2';
            this.firstElementChild.innerText = 'O';
            firstPlayerTurn = !firstPlayerTurn;
        }
        checkWin(cells);
        checkTie(cells);    
    }
}

function aiTurnEasy() {
    if (winner === null) {
        let rnd = getRnd(0,8);
        if (cells[rnd].empty === true) {
            cells[rnd].empty = !cells[rnd].empty;
            cells[rnd].owner = 'ai';
            cells[rnd].firstElementChild.innerText = 'O';
        } else aiTurnEasy();
    }
}

function checkTie (cells) {
    let counter = 0;
    // for ( let key of cells ) {
    for ( let i = 0; i < cells.length; i++) {
        if ( typeof cells[i].owner === 'string') {
            counter++;
        }
    }
    if ( winner === null && counter === cells.length ) {
        winner = 'tie';
        removeEvents();
        endGame(winner);
        // startGame();
    } else {
        counter = 0;
    }
}

function checkWin(cells) {
    for ( let i = 0; i < winCombos.length; i++) {
        let combo = winCombos[i];
        if ( cells[combo[0]].owner === cells[combo[1]].owner && cells[combo[1]].owner === cells[combo[2]].owner && cells[combo[0]].owner != null ) {
            winner = cells[combo[0]].owner;
            win();
        }
    }
}

function win() {
    removeEvents();
    endGame(winner);
    // startGame();
}

function getRnd(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function toggleAiGame () {
    aiGame = true;
    startGame();
}

function toggleHumanGame () {
    aiGame = false;
    startGame();
}

function removeEvents() {
    for ( cell of cells ) {
        cell.removeEventListener('click', click);
    }
}

function endGame (winner) {
    menu.style.display = 'block';
    if (winner == 'tie') {
        alert.innerText = "IT IS A TIE!"
    } else if (winner == 'player1') {
        alert.innerText = "Player 1 has won!"
    } else if (winner == 'player2') {
        alert.innerText = "Player 2 has won!"
    }else if (winner == 'ai') {
        alert.innerText = "AI has won the game!"
    }
    menu.appendChild(alert);
}
