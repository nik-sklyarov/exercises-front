let form = document.getElementById("form");

let firstName = document.getElementById("firstName");
let secondName = document.getElementById("secondName");
let email = document.getElementById("email");
let adress = document.getElementById("adress");
let phoneNumber = document.getElementById("phoneNumber");

let firstNameError = document.querySelector(".firstNameError");
let secondNameError = document.querySelector(".secondNameError");
let emailError = document.querySelector(".emailError");
let adressError = document.querySelector(".adressError");
let phoneNumberError = document.querySelector(".phoneNumberError");

let submit = document.getElementById("submit");

form.addEventListener("submit", function(event) {
    if (!firstName.validity.valid) {
        firstNameError.innerHTML = "You have a name, right?";
        // firstNameError.className = "firstNameError active";
        event.preventDefault();
    }
    if (!secondName.validity.valid) {
        secondNameError.innerHTML = "Don't believe You have no surname!";
        // secondNameError.className = "secondNameError active";
        event.preventDefault();
    }
    if (!email.validity.valid) {
        emailError.innerHTML = "You can't live without e-mail in 2019..";
        // emailError.className = "emailError active";
        event.preventDefault();
    }
    if (!adress.validity.valid) {
        adressError.innerHTML = "Don't pretend to be a hobo!";
        // adressError.className = "adressError active";
        event.preventDefault();
    }
    if (!phoneNumber.validity.valid) {
        phoneNumberError.innerHTML = "Find a Mobile Operator or enter THE PHONE RIGHT NOW!!!";
        // phoneNumberError.className = "phoneNumberError active";
        event.preventDefault();
    }
});