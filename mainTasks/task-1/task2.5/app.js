var firstInput = new Input({
    label: "First Name",
    id: "firstName",
    type: "text",
    pattern: "[A-Za-z]{2,}",
    placeholder: "First Name",
    spanClass: "firstName"

});

var secondInput = new Input({
    label: "Second Name",
    id: "secondName",
    type: "text",
    pattern: "[A-Za-z]{2,}",
    placeholder: "Second Name",
    spanClass: "secondName"

});

var collection = new Form();

collection.add(firstInput);
collection.add(secondInput);

$(function(){
    collection.appendTo($('body'));
});