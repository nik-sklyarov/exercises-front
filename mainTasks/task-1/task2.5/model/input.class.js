function Input(config) {
    var config = config || {};

    this.template = "<li><label><%=label%></label></br><input id=<%=id%> type=<%=type%> pattern=<%=pattern%> placeholder=<%=placeholder%> required><span class=<%=spanClass%>Error aria-live='polite'></span></li>"; // NEW

    this.appendTo = function(element) {
        var parcedTemplate = this.template
            .replace('<%=label%>', this.getLabel())
            .replace('<%=id%>', this.getId())
            .replace('<%=type%>', this.getType())
            .replace('<%=pattern%>', this.getPattern())
            .replace('<%=placeholder%>', this.getPlaceholder())
            .replace('<%=spanClass%>', this.getSpanClass());

        element.append(parcedTemplate);
    }

    this.getLabel = function() {
        return config.label;
    }
    this.getId = function() {
        return config.id;
    }
    this.getType = function() {
        return config.type;
    }
    this.getPattern = function() {
        return config.pattern;
    }
    this.getPlaceholder = function() {
        return config.placeholder;
    }
    this.getSpanClass = function() {
        return config.spanClass;
    }

    return this;
}