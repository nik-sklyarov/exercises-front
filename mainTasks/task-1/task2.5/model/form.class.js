function Form(config) {
    var config = config;
    var storage = [];
    var template = "<form><ul class='form'></ul></br><button type='submit'>Submit</button><form>";

    this.add = function(input) {
        storage.push(input);
    }

    this.appendTo = function(element) {
        let inputs = $(template);
        storage.forEach(function(one) {
            one.appendTo(inputs);
        });
        element.append(inputs);
    }

    return this;
}